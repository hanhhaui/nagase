
import React from 'react';
import validate from './validateInfo';
import useForm from './useForm';
import './Login.css';

const FormSignup = ({ submitForm }) => {
  const { handleChange, handleSubmit, values, errors } = useForm(
    submitForm,
    validate
  );
    return (
      <div className='all'>
        <form onSubmit={handleSubmit} className='form' noValidate>
          <header>
            <div className='header-logo'>  
            </div>
          </header>
          <div className='above'></div>
          <section>
            <div className='login'>
              <div className='content'>
                <p className='text'>Wellcome back!</p>
                <p className='text-small'>Enter your NVN code and password to sign in.</p>
                <div className='user'>
                  <p className='name' >User name</p>
                  <input 
                        placeholder='Username' 
                        name='username' 
                        type='text'  
                        value={values.username}
                        onChange={handleChange} />
                  {errors.username && <p className='error-text'>{errors.username}</p>}
                  <p className='pass'>Pass Word</p>
                  <input 
                          placeholder='Password' 
                          name='password' 
                          type='text' 
                          value={values.password}
                          onChange={handleChange} />
                  {errors.password && <p className='error-text'>{errors.password}</p>}
                  <div className='accept'>
                    <label class='switch'>
                      <input type='checkbox' checked/>
                      <span class='slider round'></span>
                    </label>
                    <p className='remember'>Remember me</p>
                  </div>
                  <button className='button-login' type='submit'>Login</button>
                </div>      
              </div>
           </div>
           </section>
          <footer></footer>
        </form>
      </div>
    );
};
export default FormSignup;