import React from 'react';

export default function Forgotpass() { 
    return (
      <div className='all'>
        <header>
          <div className='header-logo'>  
          </div>
        </header>
        <div className='above'>        
        </div>
        <section>
            <div className='reset'>
                <div className='content-reset'>
                    <div className='create-pas'>Reset Passsword</div>
                    <p>Email *</p>
                    <input 
                        placeholder='Example@gmail.com' 
                        name='reset-pass' 
                        type='text'  
                    />
                    <div className='buttons'>
                        <button className='button-cancel' type='submit'>Cancel</button>
                        <button className='button-submit' type='submit'>Submit</button>
                    </div>
                </div>
            </div>
        </section>   
        <footer></footer>
      </div>
    );
}